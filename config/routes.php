<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

use App\Controller\MessageQueueController;
use App\Controller\JWTController;
use App\Controller\IndexController;
use App\Middleware\Auth\JwtMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::post('/v1/create-token', [JWTController::class, 'create']);
Router::addGroup(
    '/v1', 
    function () {
        Router::post('/message-queue/send', [MessageQueueController::class, 'sendData']);
        Router::get('/init', [IndexController::class, 'index']);
    },
    ['middleware' => [JwtMiddleware::class]]
);

