<?php

declare(strict_types=1);

namespace App\Library;

use Firebase\JWT\JWT;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Di\Annotation\Inject;

class JwtLibrary
{
    /**
     * @var ConfigInterface
     */
    #[Inject]
    private $config;

    public function createToken(array $payload): string
    {
        $privateKey = $this->getPrivateKey();
        $jwt = JWT::encode($payload, $privateKey, 'RS256');
        return $jwt;
    }

    private function getPrivateKey(): string
    {
        $privateKey = $this->config->get('jwt.privateKey');
        $keyFormat = '-----BEGIN RSA PRIVATE KEY-----' . PHP_EOL;
        $keyFormat .= chunk_split($privateKey, 64, PHP_EOL);
        $keyFormat .= '-----END RSA PRIVATE KEY-----';
        return $keyFormat;
    }
}