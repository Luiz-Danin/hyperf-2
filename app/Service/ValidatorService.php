<?php

declare(strict_types=1);

namespace App\Service;

use Hyperf\Di\Annotation\Inject;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;
use Hyperf\HttpServer\Contract\RequestInterface as Request;

class ValidatorService
{
    #[Inject]
    protected ValidatorFactoryInterface $validationFactory;

    public function validatorQueueData(Request $request): ?array
    {
        $validator = $this->validationFactory->make(
            $request->all(),
            [
                'codusuario' =>'required|max:20',
                'senha' => 'required',
            ],
            [
                'codusuario.required' =>'The :attribute field is required',
                'codusuario.max' =>'The :attribute must have at most :max characters',
                'senha.required' =>'The :attribute field is required',
            ]
        );
        if ($validator->fails()){
            return $validator->errors()->getMessages();
        }
        return null;
    }
}