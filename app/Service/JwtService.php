<?php

declare(strict_types=1);

namespace App\Service;

use App\Library\JwtLibrary;
use Hyperf\Di\Annotation\Inject;
use DateTime;

class JwtService
{
    /**
     * @var JwtLibrary
     */
    #[Inject]
    private $jwtLibrary;

    public function createTokenJwt(): string
    {
        $payload = [
            'iss' => 'example.org',
            'aud' => 'example.com',
            'iat' => 1356999524,
            'nbf' => 1357000000,
            'exp' => (new DateTime("now"))->getTimestamp() + (5 * 60),
            'uid' => 1,
        ];
        return $this->jwtLibrary->createToken($payload);
    }
}