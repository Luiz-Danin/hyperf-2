<?php

declare(strict_types=1);

namespace App\Service;

use Hyperf\HttpServer\Contract\RequestInterface as Request;

class MessageQueueService
{
    /**
     * @var ValidatorService
     */
    protected $validatorService;

    public function __construct(ValidatorService $validatorService)
    {
        $this->validatorService = $validatorService;
    }

    public function publish(Request $request)
    {
        $isError = $this->validatorService->validatorQueueData($request);
        if($isError){
            return $isError;
        }
        //TO DO: logic for publishing the record
        return 'Record sent successfully';
    }
}