<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\MessageQueueService;
use Hyperf\HttpServer\Contract\RequestInterface;
use App\Request\RecordRequest;
use Hyperf\HttpServer\Contract\ResponseInterface;
use Throwable;

class MessageQueueController
{

    /**
     * @var MessageQueueService
     */
    protected $messsageQueueService;

    public function __construct(MessageQueueService $messsageQueueService)
    {
        $this->messsageQueueService = $messsageQueueService;
    }

    public function sendData(RequestInterface $request, ResponseInterface $response)
    {
        try{
            $returnService = $this->messsageQueueService->publish($request);
            return $response
                    ->json(['message' => $returnService])
                    ->withStatus(202);
        } catch(Throwable $th){
            //log in case of fatal error
            return $response
                    ->json(['message' => $th->getMessage()])
                    ->withStatus(500);
        }
    }

   
}
