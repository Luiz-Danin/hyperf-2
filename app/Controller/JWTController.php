<?php

declare(strict_types=1);

namespace App\Controller;

use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;
use App\Service\JwtService;
use Throwable;

class JWTController
{
    /**
     * @var JwtService
     */
    private $jwtService;

    public function __construct(JwtService $jwtService)
    {
        $this->jwtService = $jwtService;
    }

    public function create(RequestInterface $request, ResponseInterface $response)
    {
        try{
            $token = $this->jwtService->createTokenJwt();
            return $response
                    ->json(['message' => $token])
                    ->withStatus(201);
        } catch(Throwable $th){
            return $response
                    ->json(['message' => $th->getMessage()])
                    ->withStatus(500);
        }
        
    }
}