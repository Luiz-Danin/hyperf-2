<?php

declare(strict_types=1);

namespace App\Middleware\Auth;

use Throwable;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\Key;
use Hyperf\Contract\ConfigInterface as HyperfContainerInterface;
use Hyperf\HttpServer\Contract\ResponseInterface as HyperfResponseInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class JwtMiddleware implements MiddlewareInterface
{
    public function __construct(protected HyperfContainerInterface $config, protected HyperfResponseInterface $response)
    {
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $token = $request->getHeaderLine('Authorization');
        if($token && preg_match('/Bearer\s(\S+)/', $token, $matches)) {
            $jwtToken = $matches[1];
            try{
                $jwtPublicSecret = $this->config->get('jwt.publicKey');
                $keyPublic = $this->formatPublicKeyAsPEM($jwtPublicSecret);
                $decodedToken = JWT::decode($jwtToken, new Key($keyPublic, 'RS256'));
                $request = $request->withAttribute('jwt', $decodedToken);
            } catch(ExpiredException $exp) {
                return $this->response->json(['message' => $exp->getMessage()])->withStatus(401);
            }
            catch(Throwable $th) {
                return $this->response->json(['message' => $th->getMessage()])->withStatus(401);
            }
            return $handler->handle($request);
        }
        return $this->response->json(['message' => 'Token not provided'])->withStatus(401);
    }

    private function formatPublicKeyAsPEM(string $key): string
    {
        $keyFormat = '-----BEGIN PUBLIC KEY-----' . PHP_EOL;
        $keyFormat .= chunk_split($key, 64, PHP_EOL);
        $keyFormat .= '-----END PUBLIC KEY-----';
        return $keyFormat;
    }
}
